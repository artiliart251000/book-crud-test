const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bookRoute = require("./routes/book");
const commentRoute = require("./routes/comment")
const authRoute = require("./routes/auth")
const dotenv = require("dotenv");
const swaggerJsDoc = require("swagger-jsdoc")
const swaggerUi = require("swagger-ui-express")


dotenv.config();
app.use(express.json());
const port = 3000

const options = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: 'Booking API',
            description: "Book's API with auth and comms"
        },
        components: {
            securitySchemes: {
                bearerAuth: {
                    type: "http",
                    description: "JWT Authorization header using the Bearer scheme",
                    scheme: "bearer",
                    bearerFormat: "JWT"
                }
            }
        },
        servers: [
            {
                url: "http://localhost:3000"
            }]
    },
    apis: ["./routes/*.js", "./config/*.js"],
}

const specs = swaggerJsDoc(options)

mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(console.log("Connected to MongoDB"))
    .catch((err) => console.log(err));

app.use("/api/books", bookRoute);
app.use("/api/comment", commentRoute);
app.use("/api/auth", authRoute)
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(specs))

app.listen(port, () => {
    console.log(`App listening on port ${port}`)
});