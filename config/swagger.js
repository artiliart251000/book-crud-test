/**
 * @swagger
 * security:
 *   - bearerAuth: []
 * tags:
 *   - name: Authorization
 *     description: Authorization tools
 *   - name: Books
 *     description: All book methods
 *   - name: Comments
 *     description: Comment tools
 * paths:
 *   /api/books/getBook:
 *     get:
 *       summary: Get a book with author or title
 *       tags:
 *         - Books
 *       parameters:
 *         - in: query
 *           name: author
 *           schema:
 *              type: string
 *           required: false
 *           example: Tolstoy
 *           description: Author surname to find
 *         - in: query
 *           name: title
 *           schema:
 *             type: string
 *           required: false
 *           example: War and peace
 *           description: Book title to find
 *       responses:
 *         '200':
 *           description: OK
 * /api/books/getAll:
 *     get:
 *       summary: Get a list of all books
 *       tags:
 *         - Books
 *       responses:
 *         '200':
 *           description: OK
 * /api/books/create:
 *     post:
 *       security:
 *          - bearerAuth: []
 *       summary: Create a book
 *       tags:
 *         - Books
 *       requestBody:
 *          description: Parameters to create book
 *          required: true
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  title:
 *                      type: string
 *                      example: lenin
 *                  author:
 *                      type: string
 *                      example: stalin
 *                  year:
 *                      type: string
 *                      example: 1843
 *            description: changes
 *       responses:
 *         '200':
 *            description: OK
 *         '500':
 *            description: Book with this parameters already created
 *         '403':
 *            description: You are not authorized
 * /api/books/get/{book_id}:
 *     get:
 *       summary: Get a book with id
 *       tags:
 *         - Books
 *       parameters:
 *         - in: path
 *           name: book_id
 *           schema:
 *              type: string
 *           required: true
 *           example: 6228b28bb0b18b95f473f58b
 *           description: Book id to find
 *       responses:
 *         '200':
 *           description: OK
 * /api/books/patch/{book_id}:
 *     patch:
 *       security:
 *          - bearerAuth: []
 *       summary: Patch book with id
 *       tags:
 *         - Books
 *       parameters:
 *         - in: path
 *           name: book_id
 *           schema:
 *              type: string
 *           required: true
 *           example: 623404e535681be41657d322
 *           description: Book id to patch
 *       requestBody:
 *          description: Parameters to change in book
 *          required: true
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  title:
 *                      type: string
 *                      example: lenin
 *                  author:
 *                      type: string
 *                      example: stalin
 *                  year:
 *                      type: string
 *                      example: 1843
 *            description: Changes
 *       responses:
 *         '200':
 *           description: OK
 *         '403':
 *           description: You are not authorized
 * /api/books/delete/{book_id}:
 *     delete:
 *        security:
 *           - bearerAuth: []
 *        summary: Delete a book with id
 *        tags:
 *          - Books
 *        parameters:
 *          - in: path
 *            name: book_id
 *            schema:
 *               type: string
 *            required: true
 *            example: 624371070b50370b5602b0f4
 *            description: Book id to patch
 *        responses:
 *          '200':
 *            description: OK
 *          '403':
 *            description: You are not authorized
 * /api/auth/registration:
 *     post:
 *       summary: Create a user
 *       tags:
 *         - Authorization
 *       requestBody:
 *          description: Parameters to create a user
 *          required: true
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  username:
 *                      type: string
 *                      example: artiliartTest
 *                  password:
 *                      type: string
 *                      example: user
 *            description: parameters to use
 *       responses:
 *         '200':
 *            description: OK
 *         '500':
 *            description: User with this parameters already created
 * /api/auth/login:
 *     post:
 *       summary: Get a Bearer token with login
 *       tags:
 *         - Authorization
 *       requestBody:
 *          description: Parameters to login
 *          required: true
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  username:
 *                      type: string
 *                      example: artiliartBoss
 *                  password:
 *                      type: string
 *                      example: user
 *            description: parameters to use
 *       responses:
 *         '200':
 *            description: OK
 *         '500':
 *            description: User with this parameters already created
 * /api/auth/{user_id}:
 *     delete:
 *       security:
 *          - bearerAuth: []
 *       summary: Delete user with id
 *       tags:
 *         - Authorization
 *       parameters:
 *         - in: path
 *           name: user_id
 *           schema:
 *              type: string
 *           required: true
 *           example: find id in all accounts
 *           description: Book id to find
 *       responses:
 *         '200':
 *           description: OK
 *         '403':
 *           description: You are not authorized
 * /api/auth:
 *      get:
 *       security:
 *          - bearerAuth: []
 *       summary: Get list of all users with admin permissions
 *       tags:
 *         - Authorization
 *       responses:
 *          '200':
 *            description: OK
 *          '403':
 *            description: You are not authorized
 * /api/comment/new:
 *      post:
 *       security:
 *          - bearerAuth: []
 *       summary: Post a new comment
 *       tags:
 *         - Comments
 *       requestBody:
 *           description: Text and id of a book for comment
 *           required: true
 *           content:
 *             application/json:
 *               schema:
 *                 type: object
 *                 properties:
 *                   text:
 *                      type: string
 *                      example: Artur best
 *                   book_id:
 *                      type: string
 *                      example: 6228b28bb0b18b95f473f58b
 *             description: Comment
 *       responses:
 *           '200':
 *             description: Ok
 *           '403':
 *             description: You are not authorized
 * /api/comment/{comment_id}:
 *     delete:
 *       security:
 *          - bearerAuth: []
 *       summary: Delete comment with id
 *       tags:
 *         - Comments
 *       parameters:
 *         - in: path
 *           name: comment_id
 *           schema:
 *              type: string
 *           required: true
 *           example: find id in comments in book
 *           description: Comment id to find
 *       responses:
 *         '200':
 *           description: OK
 *         '403':
 *           description: You are not authorized
 * /api/comment/{book_id}:
 *      get:
 *        summary: Get list of comment to 1 book
 *        tags:
 *          - Comments
 *        parameters:
 *          - in: path
 *            name: book_id
 *            schema:
 *               type: string
 *            required: true
 *            example: 6228b28bb0b18b95f473f58b
 *            description: Book id to find
 *        responses:
 *           '200':
 *              description: OK
 */