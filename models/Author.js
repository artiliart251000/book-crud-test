const mongoose = require('mongoose')

const PostSchema = new mongoose.Schema({
    Surname: {
        type: String,
        required: true,
        unique: false,
    },
    Name: {
        type: String,
        required: true,
        unique: false,
    },
    Patronymic: {
        type: String,
        required: false,
        unique: false,
    },
    Born: {
        type: Number,
        required: true,
        unique: false,
    }
})

module.exports = mongoose.model("Author", PostSchema)