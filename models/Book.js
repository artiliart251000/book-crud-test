const mongoose = require('mongoose')

const CategorySchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        unique: false
    },
    year: {
        type: String,
        required: true,
        unique: false,
    },
    author: {
        type: String,
        required: true,
        unique: false,
    }
})


module.exports = mongoose.model("Book", CategorySchema)