const mongoose = require('mongoose')

const comment = new mongoose.Schema({
    username: {
        type: String,
        required: true,
    },
    text: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now(),
    },
    book_id: {
        type: String,
        required: true,
    }
})

module.exports = mongoose.model("comment", comment)