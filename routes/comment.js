const router = require("express").Router();
const Comment = require("../models/Comment");
const jwt = require("jsonwebtoken")
const {secret} = require('../config')
const roleController = require("../controllers/roleController");

router.post("/new", async (req, res) => {
    const token = req?.headers?.authorization?.split(' ')[1]
    if (!token) {
        return res.status(500).json({message: "Вставьте токен"})
    }
    const decodedUsername = jwt.verify(token, secret)
    const {text, book_id} = req.body
    const newComment = new Comment({username: decodedUsername.username, text, book_id})
    try {
        const savedComment = await newComment.save();
        res.status(200).json(savedComment);
    } catch (err) {
        res.status(500).json(err);
    }
});

router.delete("/:id", roleController(['ADMIN']), async (req, res) => {
    try {
        const comment = await Comment.findById(req.params.id);
        await comment.remove()
        res.status(200).json({message: "deleted"})
    } catch (err) {
        res.status(500).json(err)
    }
})

router.get("/:id", async (req, res) => {
    try {
        const comments = await Comment.find({"book_id": req.params.id})
        res.status(200).json(comments);
    } catch (err) {
        res.status(500).json(err);
    }
});

module.exports = router;