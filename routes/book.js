const router = require("express").Router();
const Book = require("../models/Book");
const Author = require("../models/Author");
const roleController = require("../controllers/roleController")


//CREATE BOOK
router.post("/create", roleController(['ADMIN']), async (req, res) => {
    console.log(req.body)
    const newBook = new Book(req.body);
    try {
        const savedBook = await newBook.save();
        res.status(200).json(savedBook);
    } catch (err) {
        res.status(500).json(err);
    }
});

//GET Book
router.get("/get/:id", async (req, res) => {
    try {
        const book = await Book.findById(req.params.id);
        res.status(200).json(book);
    } catch (err) {
        res.status(500).json(err);
    }
});

//Update Book
router.patch("/patch/:id", roleController(['ADMIN']), async (req, res) => {
    console.log(req.body)
    try {
        const book = await Book.findById(req.params.id);
        Object.assign(book, req.body);
        book.save();
        res.status(200).json(book)
    } catch (err) {
        res.status(500).json(err)
    }
});

//Delete Book
router.delete("/delete/:id", roleController(['ADMIN']), async (req, res) => {
    try {
        const book = await Book.findById(req.params.id);
        await book.remove()
        res.status(200).json({data: true})
    } catch (err) {
        res.status(500).json(err)
    }
})

//Get all books
router.get("/getAll", async (req, res) => {
    try {
        const books = await Book.find({}, 'title author year');
        console.log(books)
        res.status(200).json(books);
    } catch (err) {
        res.status(500).json(err);
    }
});

//GET Books with author or title
router.get("/getBook", async (req, res) => {
    const author = req.query.author;
    const title = req.query.title;
    try {
        let books;
        if (title) {
            books = await Book.find({title});
        } else if (author) {
            books = await Book.find({author});
        } else {
            books = await Author.find();
        }
        res.status(200).json(books);
    } catch (err) {
        res.status(500).json(err);
    }
});

module.exports = router;