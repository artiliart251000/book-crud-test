const router = require("express").Router();
const User = require("../models/User")
const Role = require("../models/Role")
const bcrypt = require('bcryptjs')
const {secret} = require("../config")
const {check} = require("express-validator")
const {validationResult} = require("express-validator")
const jwt = require("jsonwebtoken")
const roleController = require("../controllers/roleController")

const generateAccessToken = (id, roles, username) => {
    const payload = {
        id,
        roles,
        username
    }
    return jwt.sign(payload, secret, {expiresIn: "24h"})
}

router.post("/registration",
    [check('username', "Имя пользователя не может быть пустым").notEmpty(),
        check('password', "Пароль должен быть больше 4 и меньше 10 символов").isLength({min: 4, max: 10}),],
    async (req, res) => {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({message: "Ошибка при регистрации", errors})
            }
            const {username, password} = req.body
            const candidate = await User.findOne({username})
            if (candidate) {
                return res.status(400).json({message: "Пользователь с таким именем уже существует"})
            }
            const hashPassword = bcrypt.hashSync(password, 7);
            //const userRole = await Role.findOne({value:"USER"})
            const userRole = await Role.findOne({value: "USER"})
            const user = new User({username, password: hashPassword, roles: [userRole.value]})
            user.save()
            return res.status(200).json("Пользователь успешно зарегистрирован")
        } catch (err) {
            res.status(500).json(err);
        }
    })

router.delete("/:id", roleController(['ADMIN']), async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        await user.remove()
        res.status(200).json({message: "deleted"})
    } catch (err) {
        res.status(500).json(err)
    }
})

router.get("/", roleController(['ADMIN']), async (req, res) => {
    try {
        const users = await User.find({});
        res.status(200).json(users);
    } catch (err) {
        res.status(500).json(err);
    }
});

router.post("/login", async (req, res) => {
    try {
        const {username, password} = req.body
        const user = await User.findOne({username})
        if (!user) {
            return res.status(400).json({message: `Пользователь ${username} не найден`})
        }
        const validPassword = bcrypt.compareSync(password, user.password)
        if (!validPassword) {
            return res.status(400).json({message: "Введен неверный пароль"})
        }
        const token = generateAccessToken(user._id, user.roles, user.username)
        return res.json({token})
    } catch (err) {
        res.status(500).json(err);
    }
})

module.exports = router;
